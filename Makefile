#
# Makefile 
# path find
#

CC=dmd
CFLAGS=-H -gc -unittest

FILES=map/map.d main.d
FILES_A_STAR=map/map.d main_a_star.d
FILES_VISUAL=map/map.d map/map_dgame.d main_visual.d
FILES_MULTIPLE_VISUAL=map/map.d map/map_dgame.d main_multiple_visual.d
FILES_Q_LEARNING=map/map.d main_q_learning.d
FILES_Q_LEARNING_VISUAL=map/map.d map/map_dgame.d main_q_learning_visual.d
FILES_Q_LEARNING_MULTIPLE_VISUAL=map/map.d map/map_dgame.d main_q_learning_multiple_visual.d
LIBS=-L-lDgame -L-lDerelictUtil -L-lDerelictGL3 -L-lDerelictSDL2 -L-lSteering -L-ldl
INCLUDE=-I${HOME}/d_apps/dgame/source -I${HOME}/d_apps/derelict_util/source -I${HOME}/d_apps/derelict_gl3/source -I${HOME}/d_apps/derelict_sdl2/source -I${HOME}/d_apps/steering
CLEAN=main main_a_star main_visual main_multiple_visual main_q_learning main_q_learning_visual main_q_learning_multiple_visual

all: main main_a_star main_visual main_multiple_visual main_q_learning main_q_learning_visual main_q_learning_multiple_visual

main: ${FILES}
	${CC} $^ -ofmain ${CFLAGS}

main_a_star: ${FILES_A_STAR}
	${CC} $^ -ofmain_a_star ${CFLAGS}

main_visual: ${FILES_VISUAL}
	${CC} $^ -ofmain_visual ${CFLAGS} ${LIBS} ${INCLUDE}

main_multiple_visual: ${FILES_MULTIPLE_VISUAL}
	${CC} $^ -ofmain_multiple_visual ${CFLAGS} ${LIBS} ${INCLUDE}

main_q_learning: ${FILES_Q_LEARNING}
	${CC} $^ -ofmain_q_learning ${CFLAGS}

main_q_learning_visual: ${FILES_Q_LEARNING_VISUAL}
	${CC} $^ -ofmain_q_learning_visual ${CFLAGS} ${LIBS} ${INCLUDE}

main_q_learning_multiple_visual: ${FILES_Q_LEARNING_MULTIPLE_VISUAL}
	${CC} $^ -ofmain_q_learning_multiple_visual ${CFLAGS} ${LIBS} ${INCLUDE}

clean: ${CLEAN}
	rm $^
