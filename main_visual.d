import std.stdio;
import std.typecons;
import core.thread;
import Dgame.Graphic;
import Dgame.Window.Window;
import Dgame.Window.Event;
import map.map;
import map.map_dgame;
import steering.point;
import steering.game_object;

void main() {
  int scene_region_pixels = 10;
  int width = 1280, height = 960;
  int frames = 60, msecs = 1000;
  double distance_accepted = 2;
  int rows = width / scene_region_pixels, cols = height / scene_region_pixels;
  Map *map = map_create(rows, cols);
  int[Size.grid_max * Size.grid_max] came_from;
  int[] row_visited;
  int[] col_visited;
  Tuple!(int, int) goal;
  Tuple!(int, int) player;
  Shape[] scene_shapes;
  Shape player_shape;
  Window window = Window(width, height, "Naviagate");
  Event event;
  Point *seek = point_create();
  Point *position = point_create();
  Point *velocity = point_create();
  map_generate(map, 0.05);
  goal = map_place_goal(map);
  writef("%s\n", goal);
  player = map_place_player(map);
  writef("%s\n", player);
  map_bfs(map, came_from, goal[0], goal[1], player[0], player[1], &map_skip_next_obstacle);
  map_extract_path(row_visited, col_visited, came_from, map, goal[0], goal[1], player[0], player[1]);
  writef("%s\n", row_visited);
  row_visited = row_visited[1..$];
  if(!row_visited.length) {
    return;
  }
  writef("%s\n", col_visited);
  col_visited = col_visited[1..$];
  if(!col_visited.length) {
    return;
  }
  map_place_path(map, row_visited, col_visited);
  map_print(map);
  scene_shapes = map_shapes(map, scene_region_pixels);
  player_shape = scene_shapes[player[0] * map.cols + player[1]];
  while(true) {
    window.clear();
    while(window.poll(&event)) {
      switch(event.type) {
        case Event.Type.Quit:
          return;
        default:
          break;
      }
    }
    seek.x = row_visited[0] * scene_region_pixels;
    seek.y = col_visited[0] * scene_region_pixels;
    position.x = player_shape.getPosition().x;
    position.y = player_shape.getPosition().y;
    game_object_seek_f(velocity, position.x, position.y, seek.x, seek.y);
    player_shape.move(velocity.x, velocity.y);
    if(point_distance_f(position.x, position.y, seek.x, seek.y) <= distance_accepted) {
      row_visited = row_visited[1..$];
      if(!row_visited.length) {
        break;
      }
      col_visited = col_visited[1..$];
      if(!col_visited.length) {
        break;
      }
    }
    foreach(shape; scene_shapes) {
      if(shape == player_shape) {
        continue;
      }
      window.draw(shape);
    }
    window.draw(player_shape);
    window.display();
    Thread.sleep(dur!("msecs")(msecs / frames));
  }
  point_destroy(&velocity);
  point_destroy(&position);
  point_destroy(&seek);
  map_destroy(&map);
}
