module map.map;

import std.random;
import std.stdio;
import std.algorithm.comparison;
import std.typecons;
import std.math;
import core.stdc.stdlib;

enum Size {grid_max = 128}
enum Reward {free = 1, path = int.max / 8, player = int.max / 4, obstacle = int.max / 2, goal = int.max};

struct Map {
  int **entries;
  int rows;
  int cols;
}

Map *map_create(int rows, int cols) {
  Map *that = cast(Map*)malloc(Map.sizeof);
  that.entries = cast(int**)malloc(rows * (int*).sizeof);
  foreach(row; 0..rows) {
    that.entries[row] = cast(int*)malloc(cols * int.sizeof);
  }
  that.rows = rows;
  that.cols = cols;
  return that;
}
void map_generate(Map *that, double prob_obstacle = 0.2) {
  // simple generate based on probability
  // can use other methods
  // e.g. fractals
  double rand, max = 1048576;
  int obstacle = Reward.obstacle, free = Reward.free;
  foreach(row; 0..that.rows) {
    foreach(col; 0..that.cols) {
      rand = uniform(0, max) / max;
      if(rand <= prob_obstacle) {
        that.entries[row][col] = obstacle;
        continue;
      }
      that.entries[row][col] = free;
    }
  }
}
Tuple!(int, int) map_place_goal(Map *that) {
  double rand, max = 1048576;
  int free = Reward.free, goal = Reward.goal, row, col;
  while(true) {
    row = uniform(0, that.rows);
    col = uniform(0, that.cols);
    if(that.entries[row][col] == free) {
      that.entries[row][col] = goal;
      return tuple(row, col);
    }
  }
}
Tuple!(int, int) map_place_player(Map *that) {
  double rand, max = 1048576;
  int free = Reward.free, row, col;
  while(true) {
    row = uniform(0, that.rows);
    col = uniform(0, that.cols);
    if(that.entries[row][col] == free) {
      that.entries[row][col] = Reward.player;
      return tuple(row, col);
    }
  }
}
void map_place_path(Map *that, int[] rows, int[] cols) {
  // place path on map
  foreach(i, row; rows) {
    if(that.entries[row][cols[i]] == Reward.player) {
      continue;
    }
    if(that.entries[row][cols[i]] == Reward.goal) {
      continue;
    }
    that.entries[row][cols[i]] = Reward.path;
  }
}
bool map_skip_next_obstacle(Map *that, int row, int col) {
  // skip region having adjacent obstacle
  int[4] row_direction = [-1, 1, 0, 0];
  int[4] col_direction = [0, 0, -1, 1];
  int row_check;
  int col_check;
  foreach(i, dir; row_direction) {
    row_check = row + dir;
    col_check = col + col_direction[i];
    if(row_check < 0 || row_check >= that.rows) {
      return false;
    }
    if(col_check < 0 || col_check >= that.cols) {
      return false;
    }
    if(that.entries[row_check][col_check] == Reward.obstacle) {
      return true;
    }
  }
  return false;
}
void map_bfs(Map *that, int[] came_from, int row_goal, int col_goal, int row_player, int col_player, bool function(Map*, int, int) estimate = null) {
  // find regions leading to goal using bfs
  if(row_goal == row_player && col_goal == col_player) {
    return;
  }
  int[] row_q;
  row_q ~= row_player;
  int[] col_q;
  col_q ~= col_player;
  int[8] row_direction = [-1, 1, 0, 0, -1, -1, 1, 1];
  int[8] col_direction = [0, 0, -1, 1, -1, 1, -1, 1];
  int row_current = -1;
  int col_current = -1;
  int row_check;
  int col_check;
  int[Size.grid_max * Size.grid_max] visited;
  while(row_q.length && col_q.length) {
    row_current = row_q[0];
    row_q = row_q[1..$];
    col_current = col_q[0];
    col_q = col_q[1..$];
    foreach(i, row_dir; row_direction) {
      row_check = row_current + row_dir;
      col_check = col_current + col_direction[i];
      if(row_check < 0 || row_check >= that.rows) {
        continue;
      }
      if(col_check < 0 || col_check >= that.cols) {
        continue;
      }
      if(visited[row_check * that.cols + col_check]) {
        continue;
      }
      if(that.entries[row_check][col_check] == Reward.obstacle) {
        continue;
      }
      if(estimate && estimate(that, row_check, col_check)) {
        continue;
      }
      visited[row_check * that.cols + col_check]++;
      row_q ~= row_check;
      col_q ~= col_check;
      came_from[row_check * that.cols + col_check] = row_current * that.cols + col_current;
    }
    if(row_current == row_goal && col_current == col_goal) {
      break;
    }
  }
  // todo if two players adjacent merge paths
  // todo just check if path mapped previously
  // todo tree from every location, just follow branch to goal
  // todo visited can be simple array
}
int euclid_distance(int x_one, int y_one, int x_two, int y_two) {
  return cast(int)sqrt(pow(x_one - x_two, 2) + cast(double)pow(y_one - y_two, 2));
  // todo return double
}
int manhattan_distance(int x_one, int y_one, int x_two, int y_two) {
  return std.math.abs(x_one - x_two) + std.math.abs(y_one - y_two);
}
void map_a_star(Map *that, int[] came_from, int row_goal, int col_goal, int row_player, int col_player, int function(int, int, int, int) estimate) {
  // find regions leading to goal using a star
  if(row_goal == row_player && col_goal == col_player) {
    return;
  }
  int[Size.grid_max * Size.grid_max] open;
  open[row_player * that.cols + col_player] = 1;
  int _open = 1;
  // open set nodes to check
  int[8] row_direction = [-1, 1, 0, 0, -1, -1, 1, 1];
  int[8] col_direction = [0, 0, -1, 1, -1, 1, -1, 1];
  int row_current = -1;
  int col_current = -1;
  int row_check;
  int col_check;
  int[Size.grid_max * Size.grid_max] g_score;
  g_score[row_player * that.cols + col_player] = that.entries[row_player][col_player];
  // moving on tiles/regions
  int[Size.grid_max * Size.grid_max] f_score;
  f_score[row_player * that.cols + col_player] = g_score[row_player * that.cols + col_player] + estimate(row_player, col_player, row_goal, col_goal);
  // g_score + estimate
  int f_score_min;
  int f_score_min_i;
  int[Size.grid_max * Size.grid_max] closed;
  while(_open > 0) {
    f_score_min = int.max;
    f_score_min_i = -1;
    foreach(i, f_sc;f_score) {
      if(!f_sc) {
        continue;
      }
      if(f_sc < f_score_min) {
        f_score_min = f_sc;
        f_score_min_i = cast(int)i;
      }
    }
    if(f_score_min_i < 0) {
      continue;
    }
    row_current = f_score_min_i / that.cols;
    col_current = f_score_min_i - row_current * that.cols;
    open[f_score_min_i] = 0;
    _open--;
    closed[f_score_min_i]++;
    foreach(i, row_dir; row_direction) {
      row_check = row_current + row_dir;
      col_check = col_current + col_direction[i];
      if(row_check < 0 || row_check >= that.rows) {
        continue;
      }
      if(col_check < 0 || col_check >= that.cols) {
        continue;
      }
      if(open[row_check * that.cols + col_check]) {
        continue;
      }
      if(closed[row_check * that.cols + col_check]) {
        continue;
      }
      if(that.entries[row_check][col_check] == Reward.player) {
        continue;
      }
      if(that.entries[row_check][col_check] == Reward.obstacle) {
        continue;
      }
      came_from[row_check * that.cols + col_check] = row_current * that.cols + col_current;
      g_score[row_check * that.cols + col_check] += g_score[row_current * that.cols + col_current];
      f_score[row_check * that.cols + col_check] = g_score[row_check * that.cols + col_check] + estimate(row_check, col_check, row_goal, col_goal);
      open[row_check * that.cols + col_check]++;
      _open++;
    }
    if(row_current == row_goal && col_current == col_goal) {
      break;
    }
  }
  // todo can use some structure for f_score to not search linear
}
void map_q_learning(Map *that, Tuple!(int, int)[] actions, int row_goal, int col_goal, int function(int, int, int, int) estimate, double discount = 0.9) {
  // use q learning (reinforcement) to find regions leading to goal
  // find policy leading from start to goal region
  // larger discount care more about immediate reward, smaller discount care less
  int[8] row_direction = [-1, 1, 0, 0, -1, -1, 1, 1];
  int[8] col_direction = [0, 0, -1, 1, -1, 1, -1, 1];
  int row_check;
  int col_check;
  int row_max;
  int col_max;
  double[Size.grid_max * Size.grid_max] rewards;
  double reward_max;
  double[Size.grid_max * Size.grid_max] q_values;
  Tuple!(int, int) region;
  foreach(row; 0..that.rows) {
    foreach(col; 0..that.cols) {
      if(that.entries[row][col] == Reward.goal) {
        rewards[row * that.cols + col] = Reward.goal;
        continue;
      }
      if(that.entries[row][col] == Reward.player) {
        rewards[row * that.cols + col] = -Reward.player;
        continue;
      }
      if(that.entries[row][col] == Reward.obstacle) {
        rewards[row * that.cols + col] = -Reward.obstacle;
        continue;
      }
      rewards[row * that.cols + col] = that.entries[row][col] + 1 / cast(double)estimate(row, col, row_goal, col_goal);
    }
  }
  foreach(row; 0..that.rows) {
    foreach(col; 0..that.cols) {
      reward_max = double.min_normal;
      row_max = -1;
      col_max = -1;
      foreach(i, row_dir; row_direction) {
        row_check = row + row_dir;
        col_check = col + col_direction[i];
        if(row_check < 0 || row_check >= that.rows) {
          continue;
        }
        if(col_check < 0 || col_check >= that.cols) {
          continue;
        }
        if(rewards[row_check * that.cols + col_check] > reward_max) {
          reward_max = rewards[row_check * that.cols + col_check];
          q_values[row * that.cols + col] = discount * reward_max;
          row_max = row_check;
          col_max = col_check;
          actions[row * that.cols + col] = tuple(row_max, col_max);
        }
      }
    }
  }
  // todo rewards in parallel
  // todo this
}
void map_q_learning_path(int[] came_from, Tuple!(int, int)[] actions, Map *map, int row_goal, int col_goal, int row_player, int col_player) {
  // find path using actions
  int row_check = row_player;
  int col_check = col_player;
  Tuple!(int, int) region;
  while(true) {
    if(row_check == row_goal && col_check == col_goal) {
      break;
    }
    region = actions[row_check * map.cols + col_check];
    came_from[region[0] * map.cols + region[1]] = row_check * map.cols + col_check;
    row_check = region[0];
    col_check = region[1];
  }
}
void map_extract_path(ref int[] row_visited, ref int[] col_visited, int[] came_from, Map *map, int row_goal, int col_goal, int row_player, int col_player) {
  int row_current, col_current, row_check, col_check;
  row_current = row_goal;
  row_visited ~= row_current;
  col_current = col_goal;
  col_visited ~= col_current;
  while(true) {
    row_current = came_from[row_current * map.cols + col_current];
    if(!row_current) {
      break;
    }
    row_check = row_current / map.cols;
    row_visited ~= row_check;
    col_check = row_current - row_check * map.cols;
    col_visited ~= col_check;
    if(row_check == row_player && col_check == col_player) {
      break;
    }
    row_current = row_check;
    col_current = col_check;
  }
  row_visited = row_visited.reverse;
  col_visited = col_visited.reverse;
}
bool map_path_exists(Map *that, int row_player, int col_player) {
  // check that path exists from player to goal
  // todo this
  // todo add other directions
  return false;
}
void map_copy(Map *that, Map *src) {
  foreach(row; 0..that.rows) {
    foreach(col; 0..that.cols) {
      that.entries[row][col] = src.entries[row][col];
    }
  }
}
void map_print(Map *that) {
  foreach(row; 0..that.rows) {
    writef("row (%d) ", row);
    foreach(col; 0..that.cols) {
      if(that.entries[row][col] == Reward.path) {
        writef("- ");
        continue;
      }
      if(that.entries[row][col] == Reward.goal) {
        writef("G ");
        continue;
      }
      if(that.entries[row][col] == Reward.player) {
        writef("P ");
        continue;
      }
      if(that.entries[row][col] == Reward.free) {
        writef("+ ");
        continue;
      }
      if(that.entries[row][col] == Reward.obstacle) {
        writef("X ");
        continue;
      }
    }
    writef("\n");
  }
}
void map_print_simple(Map *that) {
  foreach(row; 0..that.rows) {
    writef("row (%d) ", row);
    foreach(col; 0..that.cols) {
      writef("%d ", that.entries[row][col]);
    }
    writef("\n");
  }
}
void map_destroy(Map **that) {
  foreach(row; 0..(*that).rows) {
    free((*that).entries[row]);
  }
  free((*that).entries);
  free(*that);
  *that = null;
}
